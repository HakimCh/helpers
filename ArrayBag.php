<?php

declare(strict_types=1);

namespace HakimCh\Helpers;

use function array_replace;
use ArrayIterator;
use function count;
use Countable;
use IteratorAggregate;

class ArrayBag implements IteratorAggregate, Countable
{
    /**
     * @var array
     */
    private $array;

    public function __construct(array $array)
    {
        $this->array = $array;
    }

    /**
     * @param mixed $key
     * @param mixed $defaultValue
     *
     * @return mixed|null
     */
    public function get($key, $defaultValue = null)
    {
        return $this->array[$key] ?? $defaultValue;
    }

    /**
     * @param mixed $key
     * @param mixed $value
     */
    public function set($key, $value): void
    {
        $this->array[$key] = $value;
    }

    /**
     * @param mixed $key
     *
     * @return bool
     */
    public function has($key): bool
    {
        return \array_key_exists($key, $this->array);
    }

    /**
     * @param mixed $key
     */
    public function remove($key): void
    {
        unset($this->array[$key]);
    }

    /**
     * @param array $array
     */
    public function add(array $array): void
    {
        $this->array = array_replace($this->array, $array);
    }

    /**
     * @param array $array
     */
    public function replace(array $array): void
    {
        $this->array = $array;
    }

    /**
     * Retrieve an external iterator.
     *
     * @return ArrayIterator
     */
    public function getIterator(): ArrayIterator
    {
        return new ArrayIterator($this->array);
    }

    /**
     * Count elements of an array.
     *
     * @return int
     */
    public function count(): int
    {
        return \count($this->array);
    }

    /**
     * @param string $path
     * @param null   $defaultValue
     *
     * @return array|mixed|null
     */
    public function getByPath(string $path, $defaultValue = null)
    {
        $result = $this->array;
        $exploded = explode('.', $path);
        while ($exploded) {
            $next = array_shift($exploded);
            if (!\array_key_exists($next, $result)) {
                return $defaultValue;
            }
            $result = $result[$next];
        }

        return $result;
    }
}
