<?php

declare(strict_types=1);

namespace HakimCh\Helpers\Http;

use App\Exceptions\UploaderExtensionException;
use Imagine\Gd\Imagine;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class Uploader.
 *
 * @deprecated
 */
class Uploader
{
    /**
     * @var string
     */
    private $baseDir;
    /**
     * @var string
     */
    private $tmpDir;
    /**
     * @var string
     */
    private $targetDir;
    /**
     * @var int
     */
    private $maxSize;
    /**
     * @var int
     */
    private $width;
    /**
     * @var int
     */
    private $height;
    /**
     * @var array
     */
    private $allowedExtensions;
    /**
     * @var File[]
     */
    private $files;

    public function __construct(string $baseDir, int $maxSize, int $width)
    {
        $this->baseDir = realpath($baseDir);
        $this->maxSize = $maxSize;
        $this->width = $width;
    }

    /**
     * @param string     $name
     * @param mixed|null $folerPath
     *
     * @return File|null
     */
    public function generateFile(string $name, $folerPath = null)
    {
        if (!file_exists($filePath = $this->getTmpDir($folerPath).basename($name))) {
            return null;
        }

        return new File($filePath);
    }

    /**
     * @param string      $name
     * @param string|null $path
     * @param mixed|null  $key
     *
     * @throws UploaderExtensionException
     *
     * @return $this
     */
    public function addFile(string $name, $key = null): self
    {
        if (!file_exists($filePath = $this->getTmpDir().$name)) {
            return $this;
        }
        $file = new File($filePath);

        if (\is_array($this->allowedExtensions) && !\in_array($file->guessExtension(), $this->allowedExtensions)) {
            throw new UploaderExtensionException(
                implode(', ', $this->allowedExtensions),
                UPLOAD_ERR_EXTENSION
            );
        }

        if ($key) {
            $this->files[$key] = $file;
        } else {
            $this->files[] = $file;
        }

        return $this;
    }

    /**
     * @param $name
     * @param string|null $path
     *
     * @return bool
     */
    public function removeFile($name, string $path = null)
    {
        if (file_exists($filename = $this->getTmpDir($path).$name)) {
            unlink($filename);

            return true;
        }

        return false;
    }

    /**
     * @param mixed|null $path
     *
     * @return array
     */
    public function upload($path = null)
    {
        $names = [];
        if (!empty($this->files)) {
            foreach ($this->files as $key => $file) {
                $names[$key] = $this->move($file, $path);
            }
        }

        return $names;
    }

    /**
     * @param File       $file
     * @param mixed|null $path
     *
     * @return string
     */
    public function move(File $file, $path = null): string
    {
        $targetDir = $path ? $this->getDirName($path) : $this->targetDir;

        $imagine = (new Imagine())->open($file->getPathname());

        $size = $imagine->getSize()->widen($this->width);
        $image = $imagine->resize($size);
        $filePath = $targetDir.$this->generateUniqueFileName($file);

        switch ($file->getExtension()) {
            case 'png':
                $image->save($filePath, ['png_compression_level' => 9]);
                break;
            case 'jpeg':
            case 'jpg':
                $image->save($filePath, ['jpeg_quality' => 90]);
                break;
            default:
                $image->save($filePath);
                break;
        }

        unlink($file->getLinkTarget());

        return $this->getAbsoluteUrl($filePath);
    }

    /**
     * @param array $files
     *
     * @throws UploaderExtensionException
     *
     * @return Uploader
     */
    public function setFiles(array $files): self
    {
        foreach ($files as $key => $fileName) {
            $this->addFile($fileName, $key);
        }

        return $this;
    }

    /**
     * @param string $tmpPath
     * @param string $targetPath
     *
     * @return Uploader
     */
    public function setTmpDir(string $tmpPath, string $targetPath): self
    {
        $this->tmpDir = $this->getDirName($tmpPath);
        $this->targetDir = $this->getDirName($targetPath);

        return $this;
    }

    /**
     * @param int $width
     * @param int $height
     *
     * @return Uploader
     */
    public function setDimensions(int $width, int $height = 0): self
    {
        $this->width = $width;
        $this->height = $height;

        return $this;
    }

    /**
     * @param array $allowedExtensions
     *
     * @return Uploader
     */
    public function setAllowedExtensions(array $allowedExtensions): self
    {
        $this->allowedExtensions = $allowedExtensions;

        return $this;
    }

    /**
     * @param string $filePath
     *
     * @return string
     */
    public function getAbsoluteUrl(string $filePath): string
    {
        $absolutePath = '/'.strstr($filePath, 'images');

        return str_replace(\DIRECTORY_SEPARATOR, '/', $absolutePath);
    }

    /**
     * @param File $file
     *
     * @return string
     */
    public function generateUniqueFileName(File $file): string
    {
        $originalName = $file instanceof UploadedFile ? $file->getClientOriginalName() : $file->getFilename();

        $explodedFilename = explode('.', $originalName);
        $fileExtension = end($explodedFilename);
        $rawFilename = str_replace('.'.$fileExtension, '', $originalName);
        $rawFilename = preg_replace('/[^a-zA-Z0-9\-\._]/', '', $rawFilename);
        $filename = str_replace(['.', '_'], '-', $rawFilename);

        return strtolower(sprintf('%s-%s.%s', $filename, uniqid(), $fileExtension));
    }

    /**
     * @param string|null $path
     *
     * @return string
     */
    private function getTmpDir(?string $path = null)
    {
        if ($path) {
            return $this->getDirName($path);
        }

        return $this->tmpDir;
    }

    /**
     * @param string $path
     *
     * @return string
     */
    private function getDirName(string $path): string
    {
        $dirName = str_replace('/', \DIRECTORY_SEPARATOR, $this->baseDir.'/'.$path);

        if (!file_exists($dirName)) {
            mkdir($dirName, 0777, true);
        }

        return trim($dirName, \DIRECTORY_SEPARATOR).\DIRECTORY_SEPARATOR;
    }
}
